package com.azeti.challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages= {"com.azeti.challenge"})
public class GreatestappapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GreatestappapiApplication.class, args);
	}

}
