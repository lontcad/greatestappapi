package com.azeti.challenge.service;

import java.io.IOException;
import java.util.List;

import com.azeti.challenge.domain.HtmlPageTitle;

public interface GreatestAppService {
 
	
	public List<String> getSearchTitle(String page_adress) throws IOException;
	public List<HtmlPageTitle> getTitleObject(String page_adress) throws IOException;
}
