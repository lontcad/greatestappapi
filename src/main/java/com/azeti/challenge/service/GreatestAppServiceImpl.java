package com.azeti.challenge.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.azeti.challenge.domain.HtmlPageTitle;
import com.azeti.challenge.util.GreatestAppHelper;

@Service
public class GreatestAppServiceImpl implements GreatestAppService{

private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	
private List<HtmlPageTitle> htmlPageTitle = new ArrayList<>();
	
	@Value("${com.azeti.parse.timeout.ms}")
	Integer timeoutInMillis;
	@Value("${com.azeti.httpsprefix}")
	String httpsSuffix;
	
	public GreatestAppServiceImpl() {
	}


	

	@Override
	public List<String> getSearchTitle(String page_adress)  {
		
		page_adress = httpsSuffix+page_adress;
		GreatestAppHelper greatestHelper = new GreatestAppHelper(page_adress, timeoutInMillis);
		
		List<String> title = new ArrayList<>();
		try {
			title.add(greatestHelper.getTitle(page_adress).get(page_adress));
		} catch (IOException e) {
			
			//e.printStackTrace();
		}
		return title;
	}


	@Override
	public List<HtmlPageTitle> getTitleObject(String page_adress){
		page_adress = httpsSuffix+page_adress;
		GreatestAppHelper greatestHelper = new GreatestAppHelper(page_adress, timeoutInMillis);
			
		try {
			htmlPageTitle.add(new HtmlPageTitle( greatestHelper.getTitle(page_adress).get(page_adress)));
		} catch (IOException e) {
			
			//e.printStackTrace();
		}
		return htmlPageTitle;
		
	}
	
	
}
