package com.azeti.challenge.domain;


import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="HtmlPageTitle", description="Giving the title of the Page Adress")
public class HtmlPageTitle implements Serializable{
	private static final long serialVersionUID = 1L;
	@ApiModelProperty("Title for the Page Adress")
	private String title;
	
	
	public HtmlPageTitle() {
		
	}
	public HtmlPageTitle(String title) {
		super();
		this.title = title;
		
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	
}