package com.azeti.challenge.restcontroller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.azeti.challenge.domain.HtmlPageTitle;
import com.azeti.challenge.service.GreatestAppService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/title")
public class GreatestAppController {

	
	
	@Autowired
	GreatestAppService greatestAppService;
	
	@ApiOperation(value = "look up the title")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation was Successfull"),
            @ApiResponse(code = 401, message = "The Resource not authorized"),
            @ApiResponse(code = 403, message = "The Access is forbidden"),
            @ApiResponse(code = 404, message = "Resource not available")
    }
    )
	
	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value="/by/{page_adress}", method = RequestMethod.GET, produces = "application/json")
	public List<HtmlPageTitle> searchObjectTitle(@PathVariable("page_adress") String page_adress) throws IOException {
		return greatestAppService.getTitleObject(page_adress);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value="/{page_adress}", method = RequestMethod.GET, produces = "application/json")
	public List<String>  searchTitle(@PathVariable("page_adress") String page_adress) throws IOException {
		return greatestAppService.getSearchTitle(page_adress);
	}
	
}


