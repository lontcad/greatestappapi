package com.azeti.challenge.util;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GreatestAppHelper {
	
private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	private String pageUrl;
	private Integer pageParseTimeoutMillis;
	
	
	public GreatestAppHelper(String pageUrl, Integer pageParseTimeoutMillis) {
		super();
		this.pageUrl = pageUrl;
		this.pageParseTimeoutMillis = pageParseTimeoutMillis;
		
	}
	
	/**
	 * Extracts Title details from meta tag using the url supplied in constructor.
	 * @param url
	 * @return
	 * @throws IOException
	 */
	public Map<String, String> getTitle(String url) throws IOException{
		Document doc = Jsoup.parse(new URL(url), pageParseTimeoutMillis);
		String title = doc.title();
		
		Map<String, String> titlePage = new HashMap<>();
		titlePage.put(url, title);
		return titlePage;
	}

}
