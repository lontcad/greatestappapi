## Anleitung
This project uses the spring boot framework for implementation the service purpose. It s a well known design pattern to separate the business logic  from the the user Interface so that it could easy to 
to change independently without any interference with the other component.
Used Technologies
1. Spring Boot Framework with Spring Tool Suite as IDE
2. Java 1.8
3. Jsoup is a very rich powerfull feature API to navigate into the HTML documents and search the HTML tags.
3. Built Tool: Maven
4. Application web Server tomcat
### Starten der Anwendung
- The application can be deployed as application server on for example tomcat, jboss etc ... 
For Running this application from eclipse you have to just right click on the Project Root and choose Run as > Spring boot Application on a Server

### Bedienung
- After deployement on the server, the service should be available on the following URL http://localhost:8082
- http://localhost:8082/swagger-ui.html

## To-Dos
Unit Tests